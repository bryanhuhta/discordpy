import random
from collections import namedtuple
from typing import Optional, List
from urllib import parse

from aiohttp import ClientSession
from discord import Embed

Definition = namedtuple('Definition', 'term definition example url')


class UrbanEvent:
    def __init__(self):
        self._url: str = 'http://urbanscraper.herokuapp.com/define/'

    async def urban_lookup(self, message: str) -> Optional[Embed]:
        """Checks if there is an urban dictionary definition."""
        # Check the message for definitions.
        definitions = []
        for phrase in UrbanEvent._split(message):
            definition = await self._get_definition(phrase)
            if definition is not None:
                definitions.append(definition)

        if len(definitions) > 0:
            # Pick a random definition.
            definition = definitions[random.randint(0, len(definitions) - 1)]
        else:
            # No results found.
            return None

        # Filter down the length of the definition if necessary.
        if len(definition.definition) > 1000:
            embed_definition = f'{definition.definition[0:1001]} ' \
                               '[... continues]'
        else:
            embed_definition = definition.definition

        embed = Embed(title=f'Urban Dictionary Definition') \
            .add_field(name='*Term*', value=definition.term, inline=False) \
            .add_field(name='*Definition*', value=embed_definition, inline=False) \
            .add_field(name='*Source*', value=definition.url, inline=False)
        return embed

    async def _get_definition(self, phrase: str) -> Optional[Definition]:
        # Perform percent-replacement and create url.
        phrase = parse.quote(phrase, safe='')
        url = parse.urljoin(self._url, phrase)

        # Perform GET request.
        async with ClientSession() as session:
            async with session.get(url) as response:
                if response.status != 200:
                    return None
                data = await response.json()

        # Give back the results.
        return Definition(term=data['term'],
                          definition=data['definition'],
                          example=data['example'],
                          url=data['url'])

    @staticmethod
    def _split(message: str) -> List[str]:
        return message.strip().split()
