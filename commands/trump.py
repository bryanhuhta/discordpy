import json
from collections import namedtuple

import requests
from discord.ext import commands
from discord.ext.commands import Context
from requests import HTTPError


Quote = namedtuple('Quote', 'message date source')


class Trump(commands.Cog):
    _url: str = 'https://api.tronalddump.io/random/quote'

    def __init__(self, bot):
        self.bot = bot

    def _get_quote(self) -> Quote:
        response = requests.get(self._url)
        if response.status_code != 200:
            raise HTTPError(response.content)
        data = json.loads(response.content.decode())

        return Quote(message=data['value'],
                     date=data['_embedded']['source'][0]['created_at'].split('T')[0],
                     source=data['_embedded']['source'][0]['url'])

    @commands.command()
    async def asktrump(self, context: Context) -> None:
        """What does Trump say?"""
        try:
            quote = self._get_quote()
        except HTTPError as e:
            print(e)
            await context.send('asktrump machine broke')
            return

        await context.send(f'> {quote.message}\n'
                           f'**Donald Trump, {quote.date}**')
