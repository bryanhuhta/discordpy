import json
import os
from collections import namedtuple
from os.path import isdir

import requests
from discord import Embed, Colour
from discord.ext import commands
from discord.ext.commands import Context
from requests import HTTPError

Answer = namedtuple('Answer', 'response image')


class YesNo(commands.Cog):
    _url: str = 'https://yesno.wtf/api'

    def __init__(self, bot):
        self._bot = bot

    def _get_answer(self) -> Answer:
        response = requests.get(self._url)
        if response.status_code != 200:
            raise HTTPError(response.content)
        data = json.loads(response.content.decode())
        return Answer(response=data['answer'], image=data['image'])

    @staticmethod
    def _color(text: str) -> Colour:
        if text.lower() == 'yes':
            return Colour.green()
        elif text.lower() == 'no':
            return Colour.red()
        else:
            return Colour.gold()

    @commands.command()
    async def ask(self, context: Context, *, question: str) -> None:
        """Ask a yes or no question."""
        try:
            title = f'**{context.author.display_name}** ' \
                    f'asks: {question}'

            if len(title) > 256:
                await context.send('`Yo son, chill with the length.`')
                return

            answer = self._get_answer()
        except HTTPError as e:
            print(e)
            await context.send('ask machine broke')
            return

        embed = Embed(title=title,
                      description=f'**{context.me.display_name}** '
                                  f'says: {answer.response}',
                      color=YesNo._color(answer.response)) \
            .set_image(url=answer.image)
        await context.send(embed=embed)
