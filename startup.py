import logging
import os
import random
from argparse import ArgumentParser, Namespace

from discord import Message
from discord.ext import commands

from commands.trump import Trump
from commands.yesno import YesNo
from events.urbanevent import UrbanEvent


def get_args() -> Namespace:
    parser: ArgumentParser = ArgumentParser()
    parser.add_argument('token',
                        metavar='token',
                        nargs='+',
                        help='The token of the bot.')
    return parser.parse_args()


def init_logger() -> None:
    if not os.path.exists('./logs'):
        os.mkdir('./logs')

    logger = logging.getLogger('discord')
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler(filename='./logs/discord.log', encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)


def start():
    init_logger()

    args = get_args()
    bot = commands.Bot(command_prefix='?')

    # Add events.
    urban_event = UrbanEvent()

    @bot.event
    async def on_ready():
        print('Logged in as \n'
              f'\tUsername: {bot.user.name}\n'
              f'\tUser Id: {bot.user.id}\n'
              '------')

    @bot.event
    async def on_message(message: Message):
        await bot.process_commands(message)
        # Skip if a bot has published the message.
        if message.author.bot:
            return None

        # Only consider some percentage of messages.
        if random.randint(0, 9) != 0:
            return None

        # Skip commands.
        if message.content[0] == bot.command_prefix:
            return None

        # Check for urban dictionary results.
        embed = await urban_event.urban_lookup(message.content)
        if embed is None:
            return None
        await message.channel.send(embed=embed)

    # Add cogs.
    bot.add_cog(YesNo(bot))
    bot.add_cog(Trump(bot))

    # Start.
    bot.run(args.token[0])


if __name__ == '__main__':
    start()
