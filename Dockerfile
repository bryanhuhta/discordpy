FROM python:3.7-alpine

COPY . /bot
WORKDIR /bot

RUN pip install -r requirements.txt

ENTRYPOINT ["python3"]
CMD []
